// (function() {
  class TplSlottedClassDemo extends HTMLElement {
    constructor() {
      super();
  
      const tpl = document.getElementById('J_tpl');
      const tplContent = tpl.content;
      const styleEle = document.createElement('style');
      styleEle.textContent = `
      ::slotted(span) {
        color: red;
      }
      `;
      const shadowDom = this.attachShadow({ mode: "open" });
      shadowDom.appendChild(styleEle);
      shadowDom.appendChild(tplContent.cloneNode(true));
    }
  }
  // 让浏览器知道自定义元素
  customElements.define("tpl-slotted-class-demo", TplSlottedClassDemo);
  // })();