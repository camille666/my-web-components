// (function() {
class TextAreaWordCount extends HTMLParagraphElement {
  constructor() {
    super();

    // 从外面传入需要计算的容器文本字数，选择器
    const ele = this.getAttribute('ele');
    const wcParent = document.getElementById(ele);

    function countWords(node) {
      const text = node.innerText || node.textContent;
      return text.split(/[(\w)(\W)]/g).length - 1;
    }
    const textEle = document.createElement("span");
    const count = `字数: ${countWords(wcParent)}`;
    textEle.textContent = count;

    const shadowDom = this.attachShadow({ mode: "open" });
    shadowDom.appendChild(textEle);

    setInterval(function() {
      const count = `字数: ${countWords(wcParent)}`;
      textEle.textContent = count;
    }, 200);
  }
}
// 让浏览器知道自定义元素
customElements.define("textarea-word-count", TextAreaWordCount, {
  extends: "p"
});
// })();
