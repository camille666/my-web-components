# 弹窗组件

1、基本功能

遮罩，文本通知，事件

2、扩展

移动，放大全屏。

3、接口

标题，可见，全屏，移动。

遮罩事件，按钮事件，拖动事件。

样式可以自定义，弹窗大小可以通过拖动上下左右边界来改变。


在外面传值：
1、窗口是否默认可见；
2、标题
3、窗口是否默认出现移动按钮；
4、窗口是否默认出现全屏按钮；
5、窗口是否默认支持全屏；
6、窗口是否默认支持移动。

### 解惑

窗口是否默认支持全屏，与，窗口是否默认出现全屏按钮是两码事，两个配置项。
默认不支持全屏，和，不出现全屏按钮。
如果设置支持全屏，但是没设置出现全屏按钮，那么无法在窗口去控制是否全屏。
如果未设置全屏，但是设置了全屏按钮，可以通过按钮去控制窗口是否全屏。

设置全屏，在于通过设置静态属性，来支持全屏。设置属性的ismove。
而设置全屏按钮，在于通过设置行为操作事件，来支持全屏。

支持，根据实际场景通过传参定制弹窗。

// todo
父组件获取弹窗组件的鼠标坐标值。
样式自定义
在卸载周期处理reset。