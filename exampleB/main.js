class TemplateShadowDomDemo extends HTMLElement {
    
    constructor() {
        super();
        const tpl = document.getElementById('J_tpl');
        const tplContent = tpl.content;
    
        tplContent.querySelector('.desc').textContent = '测试测试测试';
        tplContent.querySelector('.img').src = '';
    
        const shadowDom = this.attachShadow({mode: 'open'});
        const cloneNode = tplContent.cloneNode(true);

        const styleEle = document.createElement('style');
        styleEle.textContent = `
        .box {
            display: inline-flex;
            width: 500px;
            height: 300px;
            border: 2px solid #f00;
        }
        .img {
            width: 300px;
            height: 300px;
        }
        .desc {
            color: #0f0;
            font-size: 18px;
        }
        `;
        shadowDom.appendChild(cloneNode);
        shadowDom.appendChild(styleEle);
    }
}

customElements.define('template-shadow-dom-demo', TemplateShadowDomDemo);
    
    