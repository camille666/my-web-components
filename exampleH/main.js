(function() {
  class AddDelItem extends HTMLElement {
    constructor() {
      super();

      const shadow = this.attachShadow({ mode: "open" });
      const listContainer = document.createElement("div");

      const title = this.title;
      const addItemText = this.addItemText;
      const listItems = this.listData;
      listContainer.classList.add("add-del-item");
      listContainer.innerHTML = `
        <style>
        li, div > div {
            display: flex;
            align-items: center;
            justify-content: space-betweent;
            margin-bottom: 10px;
        }
        button {
            margin-left: 10px;
        }
        .add-del-item {
            transform: translateX(40%);
        }
        .ul-box {
            padding-inline-start: 0;
        }
        .icon {
            float: right;
            background-color: green;
            border: 1px solid #f00;
            cursor: pointer;
            font-size:20px;
        }
        </style>
        <h3>${title}</h3>
        <ul class="ul-box">
        ${listItems &&
          listItems
            .map(
              item =>
                `<li>${item}<button class="remove-item-btn icon">&ominus;</button></li>`
            )
            .join("")}
        </ul>
        <div>
        <label>${addItemText}</label>
        <input class="add-item-input" type="text"></input>
        <button class="add-item-btn icon">&oplus;</button>
        </div>
        `;

      // 绑定方法
      this.addListItem = this.addListItem.bind(this);
      this.handleRemoveItemListeners = this.handleRemoveItemListeners.bind(
        this
      );
      this.removeListItem = this.removeListItem.bind(this);

      shadow.appendChild(listContainer);
    }

    get title() {
      return this.getAttribute("title") || "";
    }
    get listData() {
      const listData = [];

      [...this.attributes].forEach(attr => {
        if (attr.name.includes("list-item")) {
          listData.push(attr.value);
        }
      });
      return listData;
    }

    get addItemText() {
      return this.getAttribute("add-item-text") || "";
    }

    // 添加某一项
    addListItem(e) {
      const textInput = this.shadowRoot.querySelector(".add-item-input");
      if (textInput.value) {
        const li = document.createElement("li");
        const button = document.createElement("button");
        const childrenLength = this.itemList.children.length;

        li.textContent = textInput.value;
        button.classList.add("remove-item-btn", "icon");
        button.innerHTML = "&ominus;";

        this.itemList.appendChild(li);
        this.itemList.children[childrenLength].appendChild(button);

        this.handleRemoveItemListeners([button]);
        textInput.value = "";
      }
    }

    removeListItem(e) {
        e.target.parentNode.remove();
    }

    handleRemoveItemListeners(arrayOfElements) {
      arrayOfElements.forEach(ele => {
        ele.addEventListener("click", this.removeListItem, false);
      });
    }
    connectedCallback() {
        const removeElementButtons = [
          ...this.shadowRoot.querySelectorAll(".remove-item-btn")
        ];
        const addElementButton = this.shadowRoot.querySelector(".add-item-btn");
  
        this.itemList = this.shadowRoot.querySelector(".ul-box");
        this.handleRemoveItemListeners(removeElementButtons);
        addElementButton.addEventListener("click", this.addListItem, false);
    }
  }
  // 让浏览器知道自定义元素
  customElements.define("add-del-item", AddDelItem);
})();
