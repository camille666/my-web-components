# exampleA

知识点：模版。
功能效果：用JavaScript在模板中插入内容，然后将其插入DOM。

# exampleB

知识点：模版+shadow dom
功能效果：渲染。

# exampleC

知识点：shadow dom
功能效果：自定义元素，使用，伪类。

# exampleD

知识点：模版和插槽。
功能效果：文本渲染。

# exampleE

知识点：模版和插槽。
功能效果：内容改变，模拟菜单路由对应内容。

# exampleF

知识点：伪类-模版-插槽。
功能效果：布局样式渲染。

# exampleG

知识点：shadow dom
功能效果：信息popover，tooltip。

# exampleH

知识点：shadow dom。
功能效果：增删某一项。

# exampleJ

知识点：shadow dom
功能效果：表单，编辑和查看。

# exampleK

知识点：shadow dom，生命周期。
功能效果：正方形的出现和消失，属性更新。

# exampleL

知识点：自定义元素，扩展ul标签。
功能效果：菜单树的展开和收起。

# exampleM

知识点：扩展段落标签。
功能效果：模拟文本域，数字计数。


# exampleN

知识点：shadow Dom。
功能效果：grid布局。


# exampleO

知识点：shadow Dom。
功能效果：弹窗，可以选择是否移动，是否放大，支持传入文本或者组件。


