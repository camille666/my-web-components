// (function() {
class EditAndScan extends HTMLElement {
  constructor() {
    super();

    const shadowRoot = this.attachShadow({ mode: "open" });
    const formEle = document.createElement("form");
    const inputEle = document.createElement("input");
    const spanEle = document.createElement("span");
    const styleEle = document.createElement("style");
    styleEle.textContent = "span {background-color: #eef; padding: 0 2px;}";
    shadowRoot.appendChild(styleEle);
    shadowRoot.appendChild(formEle);
    shadowRoot.appendChild(spanEle);
    spanEle.textContent = this.textContent;
    inputEle.value = this.textContent;

    formEle.appendChild(inputEle);
    formEle.style.display = "none";
    spanEle.style.display = "inline-block";
    inputEle.style.width = spanEle.clientWidth + "px";

    this.setAttribute("tabindex", 0);
    inputEle.setAttribute("required", "required");
    this.style.display = "inline-block";

    // 切换成编辑模式。
    function switchEditMode() {
      spanEle.style.display = "none";
      formEle.style.display = "inline-block";
      inputEle.focus();
      inputEle.setSelectionRange(0, inputEle.value.length);
    }

    // 切换成查看模式。
    function switchScanMode() {
      spanEle.style.display = "inline-block";
      formEle.style.display = "none";
      spanEle.textContent = inputEle.value;
      inputEle.style.width = spanEle.clientWidth + "px";
    }

     // 监听组件点击
     this.addEventListener("click", switchEditMode);

     // 监听表单提交
     formEle.addEventListener("submit", e => {
       switchScanMode();
       e.preventDefault();
     });
 
     // 监听文本框失去焦点
     inputEle.addEventListener("blur", switchScanMode);
  }
}
// 让浏览器知道自定义元素
customElements.define("edit-and-scan", EditAndScan);
// })();
