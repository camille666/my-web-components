// (function() {
class LifeCycleDemo extends HTMLElement {
  static get observedAttributes() {
    return ["color", "size"];
  }
  constructor() {
    super();

    const shadowDom = this.attachShadow({ mode: "open" });
    const styleEle = document.createElement("style");
    const divEle = document.createElement("div");
    shadowDom.appendChild(styleEle);
    shadowDom.appendChild(divEle);
  }

  updateStyle(elem) {
    const shadow = elem.shadowRoot;
    shadow.querySelector("style").textContent = `
  div {
    width: ${elem.getAttribute("size")}px;
    height: ${elem.getAttribute("size")}px;
    background-color: ${elem.getAttribute("color")};
  }
  `;
  }

  connectedCallback() {
    console.log("在dom添加正方形");
    this.updateStyle(this)
  }

  disconnectedCallback() {
    console.log("从dom移除正方形");
  }

  adoptedCallback() {
    console.log("正方形移到新页面");
  }

  attributeChangedCallback(name, oldValue, newValue) {
    console.log("元素属性发生改变");
    this.updateStyle(this)
  }
}
// 让浏览器知道自定义元素
customElements.define("life-cycle-demo", LifeCycleDemo);
// })();