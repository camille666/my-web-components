// (function() {
class TemplateDemo extends HTMLElement {
  constructor() {
    super();

    const tpl = document.getElementById('J_p');
    const tplContent = tpl.content;
    const shadowDom = this.attachShadow({ mode: "open" });
    shadowDom.appendChild(tplContent.cloneNode(true));
  }
}
// 让浏览器知道自定义元素
customElements.define("template-demo", TemplateDemo);
// })();