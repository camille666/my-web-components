// (function() {
class RouteSlotDemo extends HTMLElement {
  constructor() {
    super();

    const tpl = document.getElementById("J_tpl");
    const templateContent = tpl.content;

    const shadowDom = this.attachShadow({ mode: "open" });
    shadowDom.appendChild(templateContent.cloneNode(true));

    const items = Array.from(this.querySelectorAll("li"));
    const descs = Array.from(this.querySelectorAll("p"));

    items.forEach(item => {
      clickRoute(item);
    });

    function clickRoute(item) {
      item.addEventListener("click", function() {
        items.forEach(item => {
          item.style.backgroundColor = "white";
        });
        descs.forEach(desc => {
          getContent(desc, item);
        });
      });
    }

    function getContent(desc, item) {
      desc.removeAttribute("slot");
      if (desc.getAttribute("data-name") === item.textContent) {
        desc.setAttribute("slot", "contentBox");
        item.style.backgroundColor = "#0f0";
      }
    }

    // 监听slot改变
    const slots = this.shadowRoot.querySelectorAll("slot");
    slots[1].addEventListener("slotchange", function(e) {
      const nodes = slots[1].assignedNodes();
      console.log(slots[1].name, nodes[0].outerHTML);
    });
  }
}
// 让浏览器知道自定义元素
customElements.define("route-slot-demo", RouteSlotDemo);
// })();
