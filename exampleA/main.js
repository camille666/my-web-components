
    const tpl = document.getElementById('J_tpl');
    const tplContent = tpl.content;

    // 方法1，将模版直接插入dom
    tplContent.querySelector('.desc').textContent = '测试测试测试';
    tplContent.querySelector('.img').src = '';

    // document.body.appendChild(tplContent);

    // 方法2，克隆模版节点，将克隆的节点插入dom，可以多次使用模版。克隆外部文档的DOM节点，是否连子节点一起克隆。
    const cloneNode = document.importNode(tplContent, true);
    document.body.appendChild(cloneNode);