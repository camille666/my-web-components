// (function() {
class ExpandMenuTree extends HTMLUListElement {
  constructor() {
    super();

    const uls = Array.from(document.querySelectorAll(":root ul"));
    uls.slice(1).forEach(ul => {
      ul.style.display = "none";
    });

    const lis = Array.from(document.querySelectorAll(":root li"));
    lis.forEach(li => {
      const childText = li.childNodes[0];
      const spanEle = document.createElement("span");
      spanEle.textContent = childText.textContent;
      childText.parentNode.insertBefore(spanEle, childText);
      childText.parentNode.removeChild(childText);
    });

    const spans = Array.from(document.querySelectorAll(":root span"));
    spans.forEach(span => {
      if (span.nextElementSibling) {
        span.style.cursor = "pointer";
        span.parentNode.setAttribute("class", "closed");
        span.onclick = expandUl;
      }
    });
    function expandUl(e) {
      const nextul = e.target.nextElementSibling;
      if (nextul.style.display === "block") {
        nextul.style.display = "none";
        nextul.parentNode.setAttribute("class", "closed");
      } else {
        nextul.style.display = "block";
        nextul.parentNode.setAttribute("class", "open");
      }
    }
  }
}
// 让浏览器知道自定义元素
customElements.define("expand-menu-tree", ExpandMenuTree, { extends: "ul" });
// })();
